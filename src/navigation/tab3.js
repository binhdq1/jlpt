import { createStackNavigator } from 'react-navigation';
import ProfileComponent from '../components/profile';


export default Tab3 = createStackNavigator(
    {
        ProfileScreen: ProfileComponent,
    },
    {
        headerMode: 'none'
    }
);

Tab3.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};
