//To define app navigation
import { createAppContainer, createSwitchNavigator} from 'react-navigation';
import AppContainer from './app-container';
import AuthLoadingScreen from './auth-loading';
import LoginScreen from "../modules/login/screens/login";
import ChooseLevel from '../modules/login/screens/choose-level';
export default AppNavigator = createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: LoginScreen,
    AppContainer: AppContainer,
    ChooseLevel: ChooseLevel
  },
  {
    initialRouteName: 'AuthLoading',
  }
));
