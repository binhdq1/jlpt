import { createStackNavigator } from 'react-navigation';
import MySchedule from '../components/my-schedule';

export default Tab4 = createStackNavigator(
    {
        MySchedule: MySchedule
    },
    {
        headerMode: 'none'
    }
);
