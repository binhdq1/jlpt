import { NavigationActions, StackActions, DrawerActions } from 'react-navigation';

let _navigator;
let previousPage;


function setTopLevelNavigator(navigatorRef) {
    _navigator = navigatorRef;
}

function reset(routeName, params) {

	_navigator.dispatch(
		StackActions.reset({
			index: 0,
			actions: [
				NavigationActions.navigate({
					type: 'Navigation/NAVIGATE',
					routeName,
					params,
				}),
			],
		}),
	);

}

function navigate(routeName, params) {
	_navigator.dispatch(
		NavigationActions.navigate({
			type: 'Navigation/NAVIGATE',
			routeName,
			params,
		}),
	);
}



function openDrawer() {
    _navigator.dispatch(
      DrawerActions.openDrawer()
    );
  }
  
  function backPage() {
    _navigator.dispatch(
      NavigationActions.back()
    );
  }

function getCurrentRoute() {
	if (!_navigator || !_navigator.state.nav) {
		return null;
	}
	return _navigator.state.nav.routes[_navigator.state.nav.index] || null;
}
/*
function getCurrentRoute(nav){
	if(Array.isArray(nav.routes)&&nav.routes.length>0){
		return getCurrentRoute(nav.routes[nav.index])
	}else {
		return nav.routeName
	}
}*/

export default {
	setTopLevelNavigator,
    navigate,
    openDrawer,
    backPage,
	reset,
	getCurrentRoute,
};
