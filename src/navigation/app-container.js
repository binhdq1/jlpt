import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';

import HomeStack from "./home-stack";
import Tab2Stack from "./tab2";
import Tab3Stack from "./tab3";
import Tab4Stack from './tab4'

import TabbarComponent from '../common/components/tab-bar'

export default AppContainer = createBottomTabNavigator({
    Home: HomeStack,
    Tab2: Tab2Stack,
    Tab3: Tab3Stack,
    Tab4: Tab4Stack
},{
    tabBarComponent: props => <TabbarComponent {...props}/>
});
