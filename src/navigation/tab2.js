import { createStackNavigator } from 'react-navigation';
import MyShift from '../components/my-shift';

export default (Tab2 = createStackNavigator(
  {
    MyShift: MyShift,
  },
  {
    headerMode: "none"
  }
));

Tab2.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};
