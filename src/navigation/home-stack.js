import { createStackNavigator } from 'react-navigation';
import Home from '@src/components/home';
import ExamScreen from '@src/components/home/exam'
import * as Color from '@src/common/constants/colors';
import TabTopHome from '../components/home/tab-top'

export default HomeStack = createStackNavigator(
    {
        Home: {
            screen: TabTopHome,
            navigationOptions: () => ({
                headerStyle: {
                    shadowOpacity: 0,
                    shadowOffset: {
                        height: 0
                    },
                    shadowRadius: 0,
                    borderBottomWidth: 0,
                    elevation: 0,
                    backgroundColor: Color.SlateGray
                }
            })
        },
        ExamScreen: {
            screen: ExamScreen,
            navigationOptions: () => ({
                headerStyle: {
                    shadowOpacity: 0,
                    shadowOffset: {
                        height: 0
                    },
                    shadowRadius: 0,
                    borderBottomWidth: 0,
                    elevation: 0,
                    backgroundColor: Color.SlateGray
                }
            })
        }
    },
    {
        headerMode: 'none'
    }
);

HomeStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};
