import React from 'react';
import {  View, Platform  } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen'

class AuthLoadingScreen extends React.Component {

  componentDidMount() {
    if(Platform.OS == 'android'){
      SplashScreen.hide();
    }
    this.props.navigation.navigate('ChooseLevel');
  }

  componentWillMount(){
  }

  render() {
    return (
      <View></View>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);

function mapDispatchToProps(dispatch) {
  let actionCreators = {  };
  let actions = bindActionCreators(actionCreators, dispatch);
  return { ...actions, dispatch };
}

function mapStateToProps(state) {
  return {
  };
}
