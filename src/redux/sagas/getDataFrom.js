import axios from 'axios';
import {put, take, call} from 'redux-saga/effects';
import * as actions from "../actions"
import {GET_DATA, LINK_URL} from "../../common/constants/strings";



let fectDataFromType=(body)=> async ()=> {
    let url  = `${LINK_URL}${GET_DATA}`
    let dataList = await axios.post(url,body)
    return dataList
}

export function* getDataFromJlpt(action) {
    try {
        let dataList = yield call(fectDataFromType(action.body))
        if (dataList.status === 200) {
            action.onSuccess(dataList,null)
        } else {
            throw new Error(dataList.error)
        }
    } catch(error) {
        action.onError(error)
    }
}