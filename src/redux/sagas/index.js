import { takeLatest,all, fork } from 'redux-saga/effects';
import * as actions from "../actions"
import {getDataFromJlpt} from "./getDataFrom";

export default function* sagas() {
    yield all([
        takeLatest(actions.GET_DATA, getDataFromJlpt)
    ])
}
