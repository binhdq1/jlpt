import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer, createTransform } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducers from '../reducers';
import sagas from '../sagas';

const persistConfig = {
    timeout: 0,
    key: 'root',
    storage,
    whitelist: [],
    blacklist: []
}


const persistedReducer = persistReducer(persistConfig, reducers);

const middleware = []
const enhancers = []
const sagaMiddleware = createSagaMiddleware();
middleware.push(sagaMiddleware);

/* ------------- Assemble Middleware ------------- */
enhancers.push(applyMiddleware(...middleware));

const store = createStore(persistedReducer, composeWithDevTools(...enhancers));
const persistor = persistStore(store);

// kick off root saga
sagaMiddleware.run(sagas);

export { persistor, store };
