import * as actions from "../actions"

const INITIAL_STATE_LOGOUT = {
	visiable: false,
}

export default (logoutModal = (state = INITIAL_STATE_LOGOUT, action) => {
	switch (action.type) {
		case actions.ACTION_OPEN_LOGOUT_MODAL:
			return {
				visiable: true,
			}
		case actions.ACTION_CLOSE_LOGOUT_MODAL:
			return {
				visiable: false,
			}
		default:
			return state
	}
})
