import { combineReducers } from 'redux';

import logoutModal from './logoutModal';
import checkKotae from './checkKotae';

export default reducers = combineReducers({
    logoutModal: logoutModal,
    checkKotae:checkKotae
});
