// logout modal
const ACTION_OPEN_LOGOUT_MODAL = '[LOGOUT] ACTION_OPEN_LOGOUT_MODAL'

const openLogoutModal = () => {
    return {
        type: ACTION_OPEN_LOGOUT_MODAL
    }
}

const ACTION_CLOSE_LOGOUT_MODAL = '[LOGOUT] ACTION_CLOSE_LOGOUT_MODAL'
const closeLogoutModal = () => {
    return {
        type: ACTION_CLOSE_LOGOUT_MODAL
    }
}

//get data
const GET_DATA = 'getDataFromBody'
 const GET_DATA_FROM =(body,onSuccess,onError)=>({type: GET_DATA,body:body,onSuccess:onSuccess,onError:onError})
 const GET_DATA_SUCESS     = "getdatasucess";
 const GET_DATA_FAILED     = "getdatafailed";
const GET_DATA_CHECK_KOTAE     = "getdatacheckkotate";

//checkKotae
const DISPACH_DATA =(i,m) =>({type:GET_DATA_CHECK_KOTAE,i,m})


export {
    ACTION_OPEN_LOGOUT_MODAL,
    ACTION_CLOSE_LOGOUT_MODAL,
    GET_DATA_CHECK_KOTAE,
    GET_DATA,
    GET_DATA_FROM,
    GET_DATA_SUCESS,
    GET_DATA_FAILED,
    DISPACH_DATA,
    openLogoutModal,
    closeLogoutModal
}
