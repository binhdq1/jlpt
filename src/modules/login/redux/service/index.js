import * as CONSTANTS from '@src/common/constants';
import { get, post } from '@src/redux/service/base';
import AsyncStorage from '@react-native-community/async-storage';
import { changedURLImage } from '@src/utilities/url';
import { ADMIN_BASE_URL } from '@src/common/constants/api-url';
import { storageConstant, storageService } from '../../../../utilities/storage.service'

export default AuthService = {
    login: async (body) => {
        let url = CONSTANTS.URL.ADMIN.LOGIN;
        let response = await post({ url, body, bearer: false });

        if (response.ok) {
            let _bodyInit = JSON.parse(response._bodyText);
            _bodyInit.userProfile.userAvatar = ADMIN_BASE_URL + '/' + changedURLImage(_bodyInit.userProfile.user.image) + '?' + new Date().getTime()
            response._bodyInit = _bodyInit;
            response._bodyInit = { ...response._bodyInit, userCredential: body };
            await AsyncStorage.setItem(storageConstant.USER_PROFILE, JSON.stringify(response._bodyInit));
            delete response._bodyInit.userCredential;
        }

        return response;
    },
    logout: async () => {
        //move to common
        storageService.removeAllData().then(result => {
            console.log('thuynv3 - result remove all data after logout: ', result);
        });
        return true;
    },

    getRoles: async () => {
        let url = CONSTANTS.URL.ADMIN.GET_PROFILE
        let response = await get({ url, bearer: true});
        if (response.ok) {
            let _bodyInit = JSON.parse(response._bodyText);
            response._bodyInit = _bodyInit;
            storageService.setData(storageConstant.USER_ROLES, response._bodyInit.result)
        }
        return response;
    }
}
