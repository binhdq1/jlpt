
const ACTION_LOGIN = '[AUTH] LOGIN';
const login = (params) => {
    return {
        type: ACTION_LOGIN,
        payload: params,
        // meta: {
        //     retry: true,
        // }
    }
}

const ACTION_AUTO_LOGIN = '[AUTH] AUTO_LOGIN';
const autoLogin = (params) => {
    return {
        type: ACTION_AUTO_LOGIN,
        payload: params
    }
}

const ACTION_LOGIN_SUCCESS = '[AUTH] LOGIN_SUCCESS';
const loginSucess = (response) => {
    return {
        type: ACTION_LOGIN_SUCCESS,
        payload: response
    }
}

const ACTION_LOGIN_FAILURE = '[AUTH] LOGIN_FAILURE';
const loginFailure = (error) => {
    return {
        type: ACTION_LOGIN_FAILURE,
        payload: error
    }
}

const ACTION_LOGOUT = '[AUTH] LOGOUT';
const logout = () => {
    return {
        type: ACTION_LOGOUT
    }
}


const ACTION_LOGOUT_SUCCESS = '[AUTH] LOGOUT_SUCCESS';
const logoutSuccess = () => {
    return {
        type: ACTION_LOGOUT_SUCCESS
    }
}


const ACTION_LOGOUT_FAILURE = '[AUTH] LOGOUT_FAILURE';
const logoutFailure = (err) => {
    return {
        type: ACTION_LOGOUT_FAILURE,
        payload: err
    }
}

export {
    ACTION_LOGIN,
    ACTION_AUTO_LOGIN,
    ACTION_LOGIN_SUCCESS,
    ACTION_LOGIN_FAILURE,

    ACTION_LOGOUT,
    ACTION_LOGOUT_SUCCESS,
    ACTION_LOGOUT_FAILURE,

    login,
    autoLogin,
    loginSucess,
    loginFailure,
    logout,
    logoutSuccess,
    logoutFailure
}
