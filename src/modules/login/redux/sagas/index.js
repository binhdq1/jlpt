import { call, put, takeLatest } from 'redux-saga/effects';
import AuthService from '../service';
import * as AuthActions from '../actions';
import { showApplicationError } from '@src/redux/actions/app-error';
import { store } from '@src/redux/config';
import {removeStateUserTask} from '@src/modules/dashboard/redux/actions';

function* login(action) {
    try {
        let responseLogin = yield call(AuthService.login, action.payload);

        if (responseLogin.ok) {
            let response = responseLogin._bodyInit;
            if (response.code) {
                let rolesResponse = yield call(AuthService.getRoles);
                response.roles = rolesResponse._bodyInit.result;
            }
            yield put(AuthActions.loginSucess(response));
        }
        else {
            store.dispatch(showApplicationError({
                visible: true,
                content: JSON.parse(responseLogin._bodyInit).message
            }));

            yield put(AuthActions.loginFailure(responseLogin));
        }
    } catch (error) {
        store.dispatch(showApplicationError({
            visible: true,
            content: error.errorMessage
        }));
        yield put(AuthActions.loginFailure(error));
    }
}

function* autoLogin(action) {
    try {
        let responseLogin = yield call(AuthService.login, action.payload);
        let response = responseLogin._bodyInit;
        if (response.code) {
            let rolesResponse = yield call(AuthService.getRoles);
            response.roles = rolesResponse._bodyInit.result;
        }
        if (responseLogin.ok) {
            yield put(AuthActions.loginSucess(response));
        } else {
            yield put(AuthActions.loginFailure(responseLogin));
        }
    } catch (error) {
        yield put(AuthActions.loginFailure(error));
    }
}

function* logout() {
    try {
        yield call(AuthService.logout);
        yield put(AuthActions.logoutSuccess());
        yield put(removeStateUserTask())
    } catch (error) {
        yield put(AuthActions.logoutFailure(error));
    }
}

export function* authWatcher() {
    yield takeLatest(AuthActions.ACTION_LOGIN, login);
    yield takeLatest(AuthActions.ACTION_AUTO_LOGIN, autoLogin);
    yield takeLatest(AuthActions.ACTION_LOGOUT, logout);
}
