import * as AuthAction from '../actions';

const INITIAL_STATE = {
    isLoggedIn: false,
    response: null,
    etask: null,
    loading: false,
    emanning:null
};

export default auth = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        //LOGIN
        case AuthAction.ACTION_LOGIN:
            return {
                ...state,
                loading: true
            }

        case AuthAction.ACTION_LOGIN_SUCCESS:
            let etask = null
            let etasks = action.payload.userProfile.accessRights.filter(item => item.module == "eTASK" && item.plantId === action.payload.userProfile.user.defaultPlantId);
            if (etasks.length == 0){
                etask = {
                    businessId: action.payload.userProfile.user.defaultBusinessId,
                    opuId: action.payload.userProfile.user.defaultOpuId,
                    plantId: action.payload.userProfile.user.defaultPlantId,
                    role: null
                }
            }else{
                etask = etasks[0]
            }
            let emanning = action.payload.userProfile.accessRights.filter(item => item.module == "eMANNING" && item.plantId === action.payload.userProfile.user.defaultPlantId);
            let eLog = action.payload.userProfile.accessRights.filter(item => item.module == "eLOG" && item.plantId === action.payload.userProfile.user.defaultPlantId);
            if (emanning.length == 0){
                emanning.push({
                    businessId: action.payload.userProfile.user.defaultBusinessId,
                    opuId: action.payload.userProfile.user.defaultOpuId,
                    plantId: action.payload.userProfile.user.defaultPlantId,
                    role: null
                })
            }
            if (eLog.length == 0){
                eLog.push({
                    businessId: action.payload.userProfile.user.defaultBusinessId,
                    opuId: action.payload.userProfile.user.defaultOpuId,
                    plantId: action.payload.userProfile.user.defaultPlantId,
                    role: null
                })
            }
            return {
                ...state,
                isLoggedIn: true,
                response: action.payload,
                etask: etask,
                loading: false,
                emanning: emanning[0],
                eLog: eLog[0]
            }

        case AuthAction.ACTION_LOGIN_FAILURE:

        return {
                ...state,
                response: action.payload,
                loading: false
            }

        case AuthAction.ACTION_AUTO_LOGIN:
            return {
                ...state,
                response: null,
                loading: true
            }


        //LOGOUT
        case AuthAction.ACTION_LOGOUT:
            return {
                ...state,
                loading: true
            }

        case AuthAction.ACTION_LOGOUT_SUCCESS:
            return {
                ...state,
                isLoggedIn: false,
                response: null,
                etask: null,
                loading: false,
                emanning: null
            }

        case AuthAction.ACTION_LOGOUT_FAILURE:
            return {
                ...state,
                response: action.payload,
                loading: false
            }

        default:
            return state;
    }
};
