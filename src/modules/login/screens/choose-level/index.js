import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

import LinearGradient from 'react-native-linear-gradient';
import * as Color from '@src/common/constants/colors';
import Global from '@src/utilities/global.variables'
import { WebView } from 'react-native-webview';

class ChooseLevel extends React.Component {
    render(){
        return(
            <View style = {styles.container}>
                <LinearGradient style={styles.container} colors={[Color.SlateGray, Color.DarkCyan]}>
                    <View style ={{flex:1}}>
                        <View style={styles.viewText}>
                            <Text style={styles.textHeader}>Chọn trình độ</Text>
                        </View>
                        <View style={{flex: 2, alignContent: 'center'}}>
                            {this.renderItem("JLPT4")}
                            {this.renderItem("JLPT3")}
                            {this.renderItem("JLPT2")}
                            {this.renderItem("JLPT1")}
                        </View>
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Text></Text>
                        </View>
                    </View>
                </LinearGradient>
            </View>
            // <WebView source={{ uri: 'https://test2pay.ghl.com/IPGSG/Payment.aspx?transactionType=SALE&pymtMethod=ANY&serviceID=SDQ&paymentID=2019110415095615491d&orderNumber=FO-b805e-001&paymentDesc=FO-b805e-001&merchantReturnURL=https%3a%2f%2fmsuk-sodakoqdelivery.azurewebsites.net%2fapi%2forder%2fhandle-order-payment&amount=222.00&currencyCode=MYR&custIP=UNKNOWN&custName=dadsad%40gmail.com&custEmail=dadsad%40gmail.com&custPhone=123123123&merchantCallBackUrl=https%3a%2f%2fsodakoq-dev.azurewebsites.net%2fapi%2forder%2fhandle-order-payment&hashValue=6efae9a47bf80ff074a65a761b3382cd724d23abe6a04e2a7e6f5bf531bede8d&param6=0f1892a5-3cb3-4f75-8e12-77e790512077' }} />
        )
    }

    renderItem = (name) => {
        return(
            <View style = {styles.itemContainer}>
                <TouchableOpacity style={styles.itemButton} onPress={() => this._onClickItem(name)}>
                    <Text style = {{textAlign: 'center', fontSize: 16}}>{name}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    _onClickItem = (name) => {
        Global.level = name
        this.props.navigation.navigate("AppContainer")
    }
}

export default ChooseLevel
