import React from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
    Text,
    TextInput
} from 'react-native';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import * as Color from '@src/common/constants/colors';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import * as String from '@src/common/constants/strings';
import AppActivityIndicator from '@src/common/controls/activity-indicator'

class LoginScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            isLoading : false
        }
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.scroll}>
                <LinearGradient style={styles.container} colors={[Color.SlateGray, Color.DarkCyan]}>
                    {this.state.isLoading && <AppActivityIndicator/>}
                    <View style={styles.top}>
                        <Text style={styles.appName}>{String.APP_NAME}</Text>
                    </View>
                    <View style={styles.bottom}>
                        <View style={styles.inputWr}>
                            <TextInput
                                style={styles.textInputUsername}
                                placeholder="Username"
                                value={this.state.username}
                                onChangeText={(text) => this._onChangeText(text, 'username')}
                                placeholderTextColor='grey'
                                returnKeyType='next'
                                autoCapitalize = 'none'
                            />
                            <TextInput
                                style={styles.textInputPassword}
                                placeholder="Password"
                                placeholderTextColor='grey'
                                value={this.state.password}
                                onChangeText={(text) => this._onChangeText(text, 'password')}
                                returnKeyType='go'
                                secureTextEntry
                                autoCapitalize = 'none'
                            />
                        </View>
                        <TouchableOpacity style={[styles.loginBtn, !this._validate() && styles.disabled]} onPress={this._onLogin}>
                            <Text style={styles.loginText}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.versionContainer}>
                        <Text style={styles.version}>Version 0.1</Text>
                    </View>
                </LinearGradient>
            </ScrollView>
        )
    }

    _onChangeText = (text, inputName) => {
        this.setState({ [inputName]: text });
    }

    _onLogin = () => {
        if (!this._validate()) return false;
        this.setState({
            isLoading: true
        })
        //dispatch login action here
        setTimeout(() => {
            this.setState({
                isLoading: false
            })
            this._navigate();
        }, 2000)
    }

    _validate = () => {
        return this.state.username != '' && this.state.password != ''
    }

    _navigate = () => {
        this.props.navigation.navigate('AppContainer');
    }
}


const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    let actionCreators = {  };
    let actions = bindActionCreators(actionCreators, dispatch);
    return { ...actions, dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
