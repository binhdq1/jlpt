import { StyleSheet } from 'react-native';
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
import * as Color from '@src/common/constants/colors';

export default styles = StyleSheet.create({
    scroll: {
        flexGrow: 1
    },
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    top: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: moderateScale(5)
    },
    appLogo: {
        width: moderateScale(120),
        height: moderateScale(120),
    },
    appName: {
        color: 'white',
        fontWeight: '500',
        fontSize: moderateScale(28)
    },
    bottom: {
        flex: 1.25,
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: moderateScale(5)
    },
    inputWr: {
        marginBottom: moderateScale(5)
    },
    textInputUsername: {
        paddingLeft: moderateScale(15),
        width: moderateScale(280),
        height: 40,
        borderTopLeftRadius: moderateScale(5),
        borderTopRightRadius: moderateScale(5),
        borderBottomColor: 'silver',
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    textInputPassword: {
        paddingLeft: moderateScale(15),
        width: moderateScale(280),
        height: 40,
        borderBottomLeftRadius: moderateScale(5),
        borderBottomRightRadius: moderateScale(5),
    },
    loginBtn: {
        backgroundColor: Color.SlateGray,
        width: moderateScale(280),
        height: moderateScale(45),
        borderRadius: moderateScale(5),
        justifyContent: 'center',
        alignItems: 'center',
        padding: moderateScale(10)
    },
    loginText: {
        color: 'white',
        fontWeight: '500',
        fontSize: moderateScale(16),
    },
    disabled: {
        backgroundColor: Color.SlateGrayDisabled
    },
    version: {
        
        padding: moderateScale(5),
        color: 'white',
        fontSize: moderateScale(10)
    },
    versionContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    }
})