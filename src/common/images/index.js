const images = {
    logout: require("@src/assets/images/logout.png"),
    uncheckbox:require("@src/assets/images/uncheckbox.png"),
    checkbox:require("@src/assets/images/checkbox.png"),
    checkfalse:require("@src/assets/images/checkfalse.png"),
    checkDefult:require("@src/assets/images/checkDefult.png"),
    checkboxdefault:require("@src/assets/images/checkboxdefault.png")

}

export const tabBarIcons = {
    active: {
        Home: require("@src/assets/images/Home-active.png"),
        Tab2: require("@src/assets/images/MySchedule-on.png"),
        Tab3: require("@src/assets/images/Myshift-active.png"),
        Tab4: require("@src/assets/images/more-active.png"),
    },
    inactive: {
        Home: require("@src/assets/images/Home-off.png"),
        Tab2: require("@src/assets/images/MySchedule-off.png"),
        Tab3: require("@src/assets/images/Myshift-off.png"),
        Tab4: require("@src/assets/images/more.png"),
    }
}

export default images;
