//New App Colors
export const LightSeaGreen = '#1ea09b';
export const DarkCyan = '#089a9a';
export const Gray = 'gray';
export const DarkGray = '#a8a6bf';
export const Silver = 'silver';
export const SlateGray = '#5c6099';
export const SlateGrayDisabled = 'rgba(92,96,153,0.5)';
export const Indianred = '#e4686e';
export const Goldenrod = '#efab27';
export const Gainsboro = '#e6e6e6';
export const DarkKakki = "#cbb166";
export const LemonChiffon = "#fff3cd";
export const LemonChiffonLight = "#fffbf0";
export const black = "#2B3940";
export const Electromagnetic = 'rgba(47, 54, 64, 1)';
export const LightGrey = "#F2F2F2";
export const DarkSlateBlue = '#575586';
export const Red = "#DC3640";
export const LightRed = "#d2202a"
export const LighterGrey = "#f8f8f8";
export const LightBlueGrey = "#dfe6ff";
export const BlueSkyLight = "#93bfeb"
export const colors = {
    headerBar: "#5C6199",
    yellowButton: '#EFAA23',
    lightBlue: "#00A19C",
    veryLightBlue: "#E5F4F5",
    purple: "#68468B",
    lightPurple: "#615E9B",
    veryLightPurple: "#A1A7C2",
    whitePurple: 'rgba(187,190,206,0)',
    redPurple: 'rgb(237,243,247)',
    veryWhitePurple: '#EBEBEB',
    black: "#2B3940",
    white: "#FFFFFF",
    yellow: "#F0CB3E",
    lightYellow: "#FFE16B",
    extraLightYellow: '#ffdb4d',
    whiteYellow: "#FFFCCF",
    lightGrey: "#F2F2F2",
    silver: "#EEEEEE",
    grey: "#B8B8B8",
    mango: "#EFAA23",
    red: "#DC3640",
    basicRed: "#FF0000",
    basicYellow: "#FFFF00",
    basicGrey: "#C0C0C0",
    green: "#008000",
    pink: "#FFC1BF",
    darkYellowGreen: "#85862A",
    skyBlue: "#14FFEF",
    lightGreen: "#CBD34C",
    darkGreen: "#51755D",
    darkGrey: "#898989",
    dayShift: "#eaac32",
    nightShift: "#615e99",
    transparent: "transparent",
    specialTasks: '#D7D7D7',
    weekend: '#D1D962',
    normalOperation: '#EBEBEB',
    susd : '#FFEBC5',
    turnAround: '#FFC1BF',
    absent: '#444444',
    textSchedule: '#656565',
    blackIndicatorView: 'rgba(0, 0, 0, 0.4)',
    yellowInList: '#EFAA23',
    disabledYellow: '#F0D59E',
    undisabledYellow: '#EFAA23',
    searchBoxGrey: '#F2F2F2',
    searchBarGrey: '#E7E7E7',
    selectedBlue: '#ECF5FE',
    selectedHeaderColor: '#FFF3CD',
    selectedTextYellow: '#9A7500',
    headerPurple: '#615E9B',
    taskCodeWhite: '#59C1BE',
    remarksYellow: '#CBD34C',
    labelGrey: '#999999',
    remarkDetailInputGrey: '#F0F0F0',
    spacingGrey: '#F7F7F7',
    text: '#333333',
    textGrey: '#CACACA',
    textBlue: '#323499'
};