export const APP_NAME = '日本語'
export const LINK_URL ="http://34.97.149.80:8000/"
export const GET_DATA="getDataFrom"
export const TITLE_KANJI_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;">問題１　＿＿＿の言葉の読み方として最もよいものを、１・２・３・４から一つ選びなさい。</span></strong></span>'
export const TITLE_Kanji_Revert_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;">問題２　＿＿＿の言葉を漢字で書くとき、最もよいものを１・２・３・４から一つ選びなさい。</span></strong></span>'
export const TITLE_Goi_SET_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;">問題３　（　　　）に入れるのに最もよいものを、１・２・３・４から一つ選びなさい。</span></strong></span>'
export const TITLE_GOI_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;">問題４　（　　　）に入れるのに最もよいものを、１・２・３・４から一つ選びなさい。</span></strong></span>'
export const TITLE_SYNONYM_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;">問題５　＿＿＿の言葉に意味が最も近いものを、１・２・３・４から一つ選びなさい。</span></strong></span>'
export const TITLE_USAGE_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;">問題６　次の言葉の使い方として最もよいものを、１・２・３・４から一つ選びなさい。</span></strong></span>'
export const TITLE_BUNPOU_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;"><span style="color: #0000ff;"><strong><span style="font-size: large;">問題７　次の文の（　　　）に入れるのに最もよいものを、１・２・３・４から一つ選びなさい。<span style="color: #0000ff;"><strong><span style="font-size: large;"> </span></strong></span></span></strong></span></span></strong></span>'
export const TITLE_KUMITATE_N2='<span style="color: #0000ff;"><strong><span style="font-size: large;"><span style="color: #0000ff;"><strong><span style="font-size: large;"><span style="color: #0000ff;"><strong><span style="font-size: large;">問題８　次の文の＿<span style="text-decoration: underline;">★</span>＿に入る最もよいものを、１・２・３・４から一つ選びなさい。</span></strong></span></span></strong></span></span></strong></span>'
export const TITLE_BUNPOU_BUN_N2='<span style="font-size: large; color: #0000ff;"><strong>問題９　次の文章を読んで、文章全体の内容を考えて、（）から（）の中に入る最もよいものを、１・２・３・４から一つ選びなさい。</strong></span>'
export const KANJI_N2='kanjiN2'
export const TITLE_RIGHT= 'チェック'
export const Kanji_Revert_N2='kanjiRevertN2'
export const Goi_SET_N2='goisetN2'
export const GOI_N2='goiN2'
export const SYNONYM_N2='synonymN2'
export const USAGE_N2='usageN2'
export const BUNPOU_N2='bunpouN2'
export const KUMITATE_N2='kumitateN2'
export const BUNPOU_BUN_N2='bunpoubunN2'
export const COUNT_TYPE1= 5
export const COUNT_TYPE2= 1

