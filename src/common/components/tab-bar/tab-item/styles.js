import { StyleSheet } from 'react-native'
import { moderateScale } from "@src/utilities/scale";
const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center'
    },
    image:{
        width: moderateScale(25),
        height: moderateScale(25)
    },
    label:{
        fontSize: moderateScale(10)
    }
    });

export default styles
