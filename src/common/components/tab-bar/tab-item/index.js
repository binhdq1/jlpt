import React, { PureComponent } from "react";
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    Alert
} from "react-native";
import {tabBarIcons} from '@src/common/images'
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import styles from './styles'

class TabbarItem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isETaskViewUser: false,
            isBelongEtask: false
        };
    }

    handlePress = (routeName, isActive) => {
        this.props.navigation.navigate(this.props.routeName);
    }

    render() {
        const { routeName, isActive} = this.props;
        let label = '';
        if (routeName === 'Home') {
            label = 'Home';
        }

        if (routeName === 'Tab2') {
            label = 'Tab2';
        }

        if (routeName === 'Tab3') {
            label = 'Tab3';
        }

        if (routeName === 'Tab4') {
            label = 'More';
        }
        const icon = tabBarIcons[isActive ? 'active' : 'inactive'][routeName];
        return (
            <View style = {styles.container}>
                <TouchableOpacity style = {{justifyContent: 'center', alignItems: 'center'}} onPress = {() => this.handlePress(routeName, isActive)}>
                    <Image style = {styles.image} source = {icon}/>
                    <Text style = {styles.label}>{label}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    componentDidMount() {
    }
}

const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = dispatch => {
    let actionCreator = {
    };
    let actions = bindActionCreators(actionCreator, dispatch);
    return { ...actions, dispatch };
};

export default connect(mapStateToProps,mapDispatchToProps)(TabbarItem);
