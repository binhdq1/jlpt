import React from 'react';
import { View, TouchableOpacity , Text} from 'react-native';
import { styles } from './styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
const _back = props => e => {
    if(props.isRefresh){
        props.navigate('EManning', {isRefresh: true})
    }else if (props.state.params && props.state.params.stateName === "uploadFileEndTask"){
        props.popToTop()
        props.navigate('EndMyTask')
    }else {
        props.dispatch({ type: 'Navigation/BACK' });
    }
}

const StackHeader = ({props,title}) => (
    <View style={[styles.container, props.transparent && { backgroundColor: 'transparent' }]}>

        <View style={styles.header}>
            {title.type && <Text style={styles.subTitle}>{title.type}</Text>}
        </View>

        { (title.ten!==null)&&
            <View style={styles.headerTitle}>
                <Text style={styles.subTitle}>{`点:${title.ten}/5`}</Text>
            </View>
        }

        {
            props.headerLeft ? props.headerLeft() :
                <TouchableOpacity style={styles.backBtnLeft} onPress={_back(props)}>
                    <View style={styles.headerLeft}>
                        <Ionicons name="ios-arrow-back" size={moderateScale(26)} color="white" />
                    </View>
                </TouchableOpacity>
        }

        {
            props.headerRight? props.headerRight():
                <TouchableOpacity style={styles.backBtnRight} onPress={title.callBack}>
                    <View style={styles.headerRight}>
                        {title.titleRight && <Text style={styles.subTitle}>{title.titleRight}</Text>}
                    </View>
                </TouchableOpacity>
            //props.headerRight && props.headerRight()
        }
    </View>
);

export default StackHeader;
