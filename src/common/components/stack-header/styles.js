import {Dimensions, StyleSheet} from "react-native";
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
import * as Color from '@src/common/constants/colors';
const {width} = Dimensions.get('window');

export const styles = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: 'transparent',//Color.SlateGray,
        flexDirection: 'row',
        alignItems: 'center',
        height: moderateScale(50),
        paddingLeft: moderateScale(15),
    },
    title: {
        marginLeft: moderateScale(15),
        color: 'white',
        fontWeight: '500',
        fontSize: moderateScale(16)
    },
    headerTitle: {
        top: moderateScale(10),
        position: 'absolute',
        right: moderateScale(0),
        left:moderateScale(0),
        bottom:moderateScale(0),
        justifyContent: 'center',
        alignItems: 'center'
    },
    header:{
      marginTop: moderateScale(10)
    },
    backBtn: {
        marginTop: moderateScale(10),
        padding: moderateScale(2)
    },
    backBtnRight: {
        padding: moderateScale(2),
        position: 'absolute',
        right: moderateScale(15),
        top:   moderateScale(20)
    },backBtnLeft: {
        padding: moderateScale(2),
        position: 'absolute',
        left: moderateScale(15),
        top:   moderateScale(15)
    },
    subTitle: {
        marginLeft: moderateScale(20),
        color: Color.colors.white,
        fontWeight: '900',
        fontSize: moderateScale(15)
    }
});