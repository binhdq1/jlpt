import { StyleSheet } from "react-native"
import { scale, verticalScale, moderateScale } from "@src/utilities/scale"
import * as Color from "@src/common/constants/colors"

export const styles = StyleSheet.create({
	container: {
		width: "100%",
		height: moderateScale(80),
		backgroundColor: "#5c619a",
		flexDirection: "row",
		paddingTop: moderateScale(31),
		paddingLeft: moderateScale(16),
		alignItems: "flex-start",
	},
	avatar: {
		width: moderateScale(32),
		height: moderateScale(32),
		borderRadius: moderateScale(16),
	},
	appNameContainer: {
		flex: 1,
		alignItems: "flex-start",
		paddingTop: moderateScale(6),
	},
	appNameText: {
		color: "white",
		fontSize: moderateScale(20),
		lineHeight: moderateScale(20),
		fontWeight: "bold",
	},
	icon: {
		width: moderateScale(18),
		height: moderateScale(20),
	},
	menuContainer: {
		flexDirection: "row",
		alignItems: "center",
	},
	menuItemContainer: {
		marginLeft: moderateScale(15),
		marginRight: moderateScale(15),
	},
	totalNotiText: {
		fontSize: moderateScale(10),
		color: "white",
		flex: 1,
		textAlign: "center",
		textAlignVertical: "center",
	},
	totalNotiContainer: {
		position: "absolute",
		top: moderateScale(-10),
		right: moderateScale(-10),
		backgroundColor: "#efaa23",
		width: moderateScale(20),
		height: moderateScale(20),
		borderRadius: moderateScale(10),
	},
	status: {
		position: "absolute",
		bottom: 0,
		right: 0,
		width: moderateScale(10),
		height: moderateScale(10),
		borderRadius: moderateScale(5),
		backgroundColor: "#00a19c",
		borderColor: "#5c619a",
		borderWidth: moderateScale(1),
	},
})
