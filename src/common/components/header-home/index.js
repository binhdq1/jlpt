import React from "react"
import { View, Image, TouchableOpacity, Text } from "react-native"
import { styles } from "./styles"
import { moderateScale } from "@src/utilities/scale"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import Icon from "react-native-vector-icons/FontAwesome"
import { colors } from "@src/common/constants/colors"
import { withNavigationFocus } from 'react-navigation';
import images from "@src/common/images"
import { openLogoutModal } from "@src/redux/actions"

class HomeHeader extends React.PureComponent {
	constructor(props) {
		super(props)

		this.state = {
			notification: [],
			title: "",
		}
	}

	componentDidMount() {
	}
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.appNameContainer}>
					<Text style={styles.appNameText}>{this.props.title}</Text>
				</View>
				<View style={styles.menuContainer}>
					<TouchableOpacity style={styles.menuItemContainer} onPress={() => this._navigateTo("Notification")}>
						<View style={styles.totalNotiContainer}>
							<Text style={styles.totalNotiText} numberOfLines={1}>
								0
							</Text>
						</View>
					</TouchableOpacity>
					<TouchableOpacity
						style={styles.menuItemContainer}
						onPress={() => this._navigateTo("UserProfile")}
					>
						<Icon name={"user"} size={moderateScale(32)} color={colors.grey} style={styles.avatar} />
						<View style={styles.status} />
					</TouchableOpacity>
					{this.props.isShowIconOut&& <TouchableOpacity
						style={styles.menuItemContainer}
						onPress={() => this._onSelectItem()}
					>
						<Image source={images.logout} style={styles.icon} />
					</TouchableOpacity>}
				</View>
			</View>
		)
	}

	_onSelectItem = () => {
		this.props.openLogoutModal()
	}
	_navigateTo = (screen) => {
		this.props.navigation.navigate(screen)
	}
}

const mapStateToProps = state => {
	return {
	}
}

const mapDispatchToProps = dispatch => {
	let actionCreators = { openLogoutModal }
	let actions = bindActionCreators(actionCreators, dispatch)
	return { ...actions, dispatch }
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(withNavigationFocus(HomeHeader))
