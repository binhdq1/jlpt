import { StyleSheet, Dimensions } from "react-native"
import { moderateScale } from "@src/utilities/scale"
const { width } = Dimensions.get("window")

const styles = StyleSheet.create({
	container: {
		justifyContent: "center",
		alignItems: "center",
	},
	modalContent: {
		backgroundColor: "white",
		borderRadius: moderateScale(5),
		width: width - moderateScale(40),
		justifyContent: "space-between",
		alignItems: "center",
	},
	avatar: {
		width: moderateScale(60),
		height: moderateScale(60),
		borderRadius: moderateScale(30),
	},
	logoutText: { margin: moderateScale(30), fontWeight: "bold", fontSize: moderateScale(25) },
	button: {
		borderRadius: moderateScale(10),
		flex: 1,
		margin: moderateScale(20),
		marginLeft: moderateScale(10),
		marginRight: moderateScale(10),
		justifyContent: "center",
		alignItems: "center",
		paddingTop: moderateScale(15),
		paddingBottom: moderateScale(15),
	},
	buttonText: {
		fontSize: moderateScale(15),
	},
	note: { margin: moderateScale(20) },
	buttonContainer: { flexDirection: "row", justifyContent: "space-around" },
})

export default styles
