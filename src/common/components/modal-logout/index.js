import React, { PureComponent } from "react"
import { TouchableOpacity, View, Image, Text } from "react-native"
import styles from "./styles"
import Modal from "react-native-modal"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { closeLogoutModal } from "@src/redux/actions"
import Icon from "react-native-vector-icons/FontAwesome"
import { moderateScale } from "@src/utilities/scale"
import { colors } from "@src/common/constants/colors"
import NavigationService from '@src/navigation/NavigatorServices';

const Button = props => (
	<TouchableOpacity
		style={[styles.button, { backgroundColor: props.color }]}
		onPress={props.onPress.bind(this)}
	>
		<View>
			<Text style={styles.buttonText}>{props.text}</Text>
		</View>
	</TouchableOpacity>
)

class ModalLogout extends PureComponent {
	renderModalContent = () => (
		<View style={styles.modalContent}>
			<Text style={styles.logoutText}>{"Log Out"}</Text>
			{this.props.profilePicUrl ? (
				<Image
					source={{ uri: this.props.profilePicUrl, cache: "only-if-cached" }}
					style={styles.avatar}
				/>
			) : (
				<Icon name={"user"} size={moderateScale(35)} color={colors.grey} />
			)}
			<Text style={styles.note}>{"Do you want to log out?"}</Text>
			<View style={styles.buttonContainer}>
				<Button text={"CANCEL"} color={colors.searchBarGrey} onPress={() => this.onPress("CANCEL")} />
				<Button text={"YES"} color={"#FAA641"} onPress={() => this.onPress("YES")} />
			</View>
		</View>
	)

	onPress = action => {
		this.props.closeLogoutModal()
		if(action === "YES"){
			NavigationService.navigate('Auth')
		}
	}

	render() {
		return (
			<Modal isVisible={this.props.logoutModal.visiable} style={styles.container}>
				{this.renderModalContent()}
			</Modal>
		)
	}
}
const mapStateToProps = state => {
	return {
		logoutModal: state.logoutModal
	}
}
const mapDispatchToProps = dispatch => {
	let actionCreators = { closeLogoutModal }
	let actions = bindActionCreators(actionCreators, dispatch)
	return { ...actions, dispatch }
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(ModalLogout)
