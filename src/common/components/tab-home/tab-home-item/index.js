import React, { PureComponent } from "react";
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    Alert
} from "react-native";
import * as Color from '@src/common/constants/colors';
import {tabBarIcons} from '@src/common/images'
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import styles from './styles'

class TabHomeItem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handlePress = (routeName, isActive) => {
        this.props.navigation.navigate(this.props.routeName);
    }

    render() {
        const { routeName, isActive} = this.props;
        let label = '';
        if (routeName === 'TabGrammar') {
            label = 'GRAMMAR';
        }

        if (routeName === 'TabRead') {
            label = 'READ';
        }

        if (routeName === 'TabVocabulary') {
            label = 'VOCABULARY';
        }
        return (
            <View style = {styles.container}>
                <TouchableOpacity style = {[styles.item, {borderBottomWidth: isActive? 2 : 0, borderBottomColor: isActive? Color.Goldenrod : Color.Gainsboro}]} onPress = {() => this.handlePress(routeName, isActive)}>
                    <Text style = {styles.label}>{label}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    componentDidMount() {
    }
}

const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = dispatch => {
    let actionCreator = {
    };
    let actions = bindActionCreators(actionCreator, dispatch);
    return { ...actions, dispatch };
};

export default connect(mapStateToProps,mapDispatchToProps)(TabHomeItem);
