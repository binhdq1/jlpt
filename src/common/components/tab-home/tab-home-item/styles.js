import { StyleSheet } from 'react-native'
import { moderateScale } from "@src/utilities/scale";
const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        marginTop: moderateScale(20)
    },
    image:{
        width: moderateScale(25),
        height: moderateScale(25)
    },
    label:{
        fontSize: moderateScale(12)
    },
    item: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
    });

export default styles
