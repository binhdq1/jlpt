import { StyleSheet } from 'react-native'
import { moderateScale } from "@src/utilities/scale";
import { isIphoneX } from 'react-native-device-detection';
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    item: {
        height: isIphoneX?moderateScale(75):moderateScale(50),
        flexDirection: 'row',
        backgroundColor:"whitesmoke",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: -1,
        },
        shadowOpacity: 1,
        shadowRadius: 1,
        paddingBottom:isIphoneX?30:0
    }
    });

export default styles
