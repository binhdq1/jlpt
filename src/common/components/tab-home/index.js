import React, { PureComponent } from "react";
import {
    View
} from "react-native";
import styles from './styles'
import TabHomeItem from './tab-home-item'
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
class TabHomeComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        const { navigation} = this.props;
        const { routes, index} = navigation.state;
        return (
            <View style = {styles.item}>
                {
                    routes.map((route, i) => (
                        <TabHomeItem navigation = {navigation} key = {route.routeName} {...route} isActive = {index === i}/>
                    ))
                }
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
    };
};

const mapDispatchToProps = dispatch => {
    let actionCreator = {
    };
    let actions = bindActionCreators(actionCreator, dispatch);
    return { ...actions, dispatch };
};

export default connect(mapStateToProps,mapDispatchToProps)(TabHomeComponent);
