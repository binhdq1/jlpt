import React, {Component} from 'react';
import {View, Text, Dimensions} from 'react-native';
import {styles} from './styles';
import HTML from 'react-native-render-html';
import {CheckBox} from "../../check-box";
import {handleCheckBox,textsStylesBehaviour1} from "../questionType/questionType";


//5. bunpoubunn2

class QuestionType5 extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        let renView = this.props.item.map((obj, i) => {
            let customQuestion = obj.question.reduce((question, item, index) => {
                return question += item
            }, "")
            let html = ('<div>' +(`${i + 1}.`)+ customQuestion.replace("text-decoration", "textDecorationLine") + '</div>');

            let questionView = obj.suggestion.map((item,i)=>{

                let style = i % 2 === 0 ? styles.container1 : styles.container;
                return <View  key={i} style={style}>
                    <View style={styles.header}>
                        <HTML tagsStyles={textsStylesBehaviour1(true,null).propsQuesetion.tagsStyles}
                              html={item.question1}
                        />
                    </View>

                    <View style={styles.header}>
                        <CheckBox
                            selected={this.props.checkList[i][0]}
                            onPress={handleCheckBox(i, 0,this.props)}
                            flag={this.props.flag}
                            selectedFlag={this.props.flag ? null : {
                                checkKoate: this.props.checkKoate[i].checkKoate[0],
                                flag: this.props.checkKoate[i].flag
                            }}/>
                        <HTML tagsStyles={this.props.flag?
                            textsStylesBehaviour1(true).props.tagsStyles:
                            textsStylesBehaviour1(false,{
                                checkKoate: this.props.checkKoate[i].checkKoate[0],
                                flag: this.props.checkKoate[i].flag
                            }).props.tagsStyles}
                              html={item.suggestion1}/>
                    </View>


                    <View style={styles.header}>
                        <CheckBox
                            selected={this.props.checkList[i][1]}
                            onPress={handleCheckBox(i, 1,this.props)}
                            flag={this.props.flag}
                            selectedFlag={this.props.flag ? null : {
                                checkKoate: this.props.checkKoate[i].checkKoate[1],
                                flag: this.props.checkKoate[i].flag
                            }}/>
                        <HTML html={item.suggestion2}
                              tagsStyles={this.props.flag?
                                  textsStylesBehaviour1(true).props.tagsStyles:
                                  textsStylesBehaviour1(false,{
                                      checkKoate: this.props.checkKoate[i].checkKoate[1],
                                      flag: this.props.checkKoate[i].flag
                                  }).props.tagsStyles}/>
                    </View>


                    <View style={styles.header}>
                        <CheckBox
                            selected={this.props.checkList[i][2]}
                            onPress={handleCheckBox(i, 2,this.props)}
                            flag={this.props.flag}
                            selectedFlag={this.props.flag ? null : {
                                checkKoate: this.props.checkKoate[i].checkKoate[2],
                                flag: this.props.checkKoate[i].flag
                            }}/>
                        <HTML html={item.suggestion3}
                              tagsStyles={this.props.flag?
                                  textsStylesBehaviour1(true).props.tagsStyles:
                                  textsStylesBehaviour1(false,{
                                      checkKoate: this.props.checkKoate[i].checkKoate[2],
                                      flag: this.props.checkKoate[i].flag
                                  }).props.tagsStyles}/>
                    </View>

                    <View style={styles.header}>
                        <CheckBox
                            selected={this.props.checkList[i][3]}
                            onPress={handleCheckBox(i, 3,this.props)}
                            flag={this.props.flag}
                            selectedFlag={this.props.flag ? null : {
                                checkKoate: this.props.checkKoate[i].checkKoate[3],
                                flag: this.props.checkKoate[i].flag
                            }}/>
                        <HTML html={item.suggestion4}
                              tagsStyles={this.props.flag?
                                  textsStylesBehaviour1(true).props.tagsStyles:
                                  textsStylesBehaviour1(false,{
                                      checkKoate: this.props.checkKoate[i].checkKoate[3],
                                      flag: this.props.checkKoate[i].flag
                                  }).props.tagsStyles}/>
                    </View>

                </View>
            })

            return <View  key={i}>

                <View style={styles.title1}>
                    <HTML tagsStyles={textsStylesBehaviour1(true,null).propsQuesetion.tagsStyles}
                          html={html}/>
                </View>

                <View>{questionView}</View>

            </View>
        })

        return (
            <View style={styles.container}>
                {renView}
            </View>
        )

    }

}

export default QuestionType5;
