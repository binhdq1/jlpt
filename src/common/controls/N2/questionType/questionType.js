import {colors} from "../../../constants/colors";
import {Dimensions} from "react-native";
const {width} = Dimensions.get('window');
import {moderateScale} from "@src/utilities/scale";
import images from "../../../images";

let checkListText=(flag, selectedFlag)=>{
    if(flag) {
        return colors.black
    } else {
        if(selectedFlag.checkKoate===null){
            return colors.black
        } else {
            if (selectedFlag.flag !== 0) {
                if (selectedFlag.flag === 1) {
                    return colors.green
                } else {
                    if (selectedFlag.checkKoate) {
                        return colors.green
                    } else {
                        return colors.red
                    }
                }
            } else {
                return colors.green
            }
        }
    }
}

 export let textsStylesBehaviour = (flag,selectedFlag)=>({
    name: 'Texts styles behaviour',
    propsQuesetion: {
        tagsStyles: {
            div: {
                width: width - moderateScale(37),
                fontSize: moderateScale(15),
                fontWeight: '300',
                fontFamily: "MS Mincho",
                lineHeight: 30
            }
        }
    },
    props: {
        tagsStyles: {
            div: {width: width - moderateScale(37),
                fontFamily: "MS Mincho",
                lineHeight: 30,
                color:checkListText(flag,selectedFlag)}
        }
    }
})

export let textsStylesBehaviour1 = (flag,selectedFlag)=>({
    name: 'Texts styles behaviour',
    propsQuesetion: {
        tagsStyles: {
            div: {
                width: width - moderateScale(37),
                fontSize: moderateScale(14),
                fontWeight: '300',
                fontFamily: "MS Mincho",
                lineHeight: 30
            }
        }
    },
    props: {
        tagsStyles: {
            div: {width: width - moderateScale(37),
                fontFamily: "MS Mincho",
                lineHeight: 30,
                color:checkListText(flag,selectedFlag)}
        }
    }
})

export let handleCheckBox = (i, m, props) => () => {
    let checkList = props.checkList;
    for (let t = 0; t < checkList[i].length; t++) {
        checkList[i][t] = false
    }
    checkList[i][m] = true;
    props.updateCheckList(checkList);
};