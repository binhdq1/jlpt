import { StyleSheet } from "react-native";
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
import { colors } from '@src/common/constants/colors';

export const styles = StyleSheet.create({
    container: {
       backgroundColor: colors.whitePurple,
    },
    container1: {
        backgroundColor: colors.redPurple,
    },
    header: {
        marginLeft:moderateScale(5),
        flexDirection: 'row',
        padding: moderateScale(5),
        alignItems: 'center'
    },
    title: {
        marginLeft:moderateScale(5),
        marginTop:moderateScale(10),
        flexDirection: 'row',
        padding: moderateScale(5),
    },
    content: {
        paddingLeft: moderateScale(10)
    },
    wrapStyle: {
        marginTop: moderateScale(5)
    }
});
