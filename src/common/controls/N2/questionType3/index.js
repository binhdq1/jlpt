import React, {Component} from 'react';
import {View,Text, Dimensions} from 'react-native';
import { styles } from './styles';
import HTML from 'react-native-render-html';
import {CheckBox} from "../../check-box";
import {textsStylesBehaviour,handleCheckBox} from "../questionType/questionType";


//3. usagen2



class QuestionType3 extends Component{
    constructor(props){
        super(props);
    }


    render() {

        let renView = this.props.item.map((obj, i) => {
            let html = ('<div>' +(`${i + 1}.`)+ obj.question + '</div>').replace("text-decoration", "textDecorationLine");
            let suggestion1 = ('<div>' + obj.suggestion1 + '</div>').replace("text-decoration", "textDecorationLine");
            let suggestion2 = ('<div>' + obj.suggestion2 + '</div>').replace("text-decoration", "textDecorationLine");
            let suggestion3 = ('<div>' + obj.suggestion3 + '</div>').replace("text-decoration", "textDecorationLine");
            let suggestion4 = ('<div>' + obj.suggestion4 + '</div>').replace("text-decoration", "textDecorationLine");
            let style = i % 2 === 0?styles.container1:styles.container;
            return <View style={style} key={i}>
                <View style={styles.title}>
                    <HTML tagsStyles={textsStylesBehaviour(true,null).propsQuesetion.tagsStyles}
                          html={html}/>
                </View>

                <View style={styles.header}>
                    <CheckBox
                        selected={this.props.checkList[i][0]}
                        onPress={handleCheckBox(i, 0,this.props)}
                        flag={this.props.flag}
                        selectedFlag={this.props.flag ? null : {
                            checkKoate: this.props.checkKoate[i].checkKoate[0],
                            flag: this.props.checkKoate[i].flag
                        }}/>
                    <HTML tagsStyles={this.props.flag?
                        textsStylesBehaviour(true).props.tagsStyles:
                        textsStylesBehaviour(false,{
                            checkKoate: this.props.checkKoate[i].checkKoate[0],
                            flag: this.props.checkKoate[i].flag
                        }).props.tagsStyles}
                          html={suggestion1}/>
                </View>

                <View style={styles.header}>
                    <CheckBox
                        selected={this.props.checkList[i][1]}
                        onPress={handleCheckBox(i, 1,this.props)}
                        flag={this.props.flag}
                        selectedFlag={this.props.flag ? null : {
                            checkKoate: this.props.checkKoate[i].checkKoate[1],
                            flag: this.props.checkKoate[i].flag
                        }}/>
                    <HTML html={suggestion2}
                          tagsStyles={this.props.flag?
                              textsStylesBehaviour(true).props.tagsStyles:
                              textsStylesBehaviour(false,{
                                  checkKoate: this.props.checkKoate[i].checkKoate[1],
                                  flag: this.props.checkKoate[i].flag
                              }).props.tagsStyles}/>
                </View>

                <View style={styles.header}>
                    <CheckBox
                        selected={this.props.checkList[i][2]}
                        onPress={handleCheckBox(i, 2,this.props)}
                        flag={this.props.flag}
                        selectedFlag={this.props.flag ? null : {
                            checkKoate: this.props.checkKoate[i].checkKoate[2],
                            flag: this.props.checkKoate[i].flag
                        }}/>
                    <HTML html={suggestion3}
                          tagsStyles={this.props.flag?
                              textsStylesBehaviour(true).props.tagsStyles:
                              textsStylesBehaviour(false,{
                                  checkKoate: this.props.checkKoate[i].checkKoate[2],
                                  flag: this.props.checkKoate[i].flag
                              }).props.tagsStyles}/>
                </View>

                <View style={styles.header}>
                    <CheckBox
                        selected={this.props.checkList[i][3]}
                        onPress={handleCheckBox(i, 3,this.props)}
                        flag={this.props.flag}
                        selectedFlag={this.props.flag ? null : {
                            checkKoate: this.props.checkKoate[i].checkKoate[3],
                            flag: this.props.checkKoate[i].flag
                        }}/>
                    <HTML html={suggestion4}
                          tagsStyles={this.props.flag?
                              textsStylesBehaviour(true).props.tagsStyles:
                              textsStylesBehaviour(false,{
                                  checkKoate: this.props.checkKoate[i].checkKoate[3],
                                  flag: this.props.checkKoate[i].flag
                              }).props.tagsStyles}/>
                </View>


            </View>
        })

        return (
            <View style={styles.container}>
                {renView}
            </View>
        )

    }

}

export default QuestionType3;
