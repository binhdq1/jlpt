import { StyleSheet } from "react-native"
import { scale, verticalScale, moderateScale } from "@src/utilities/scale"
import * as Color from "@src/common/constants/colors"


export const styles = StyleSheet.create({
    checkstyle: {
        width: moderateScale(25),
        height:moderateScale(25)
    },

});
