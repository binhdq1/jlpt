import React from 'react';
import {Image, TouchableOpacity} from "react-native";
import {styles} from "./styles"
import images from "../../images"


export const CheckBox = ({selected,onPress,flag,selectedFlag}) => (
    <TouchableOpacity style={styles.checkstyle} onPress={onPress}>
        {flag? <Image style={styles.checkstyle} source={selected ?images.checkboxdefault:images.uncheckbox }></Image>
        : <Image style={styles.checkstyle} source={(selectedFlag.checkKoate===null)?images.uncheckbox:checkListBox(selectedFlag)}></Image>}
    </TouchableOpacity>
)


let checkListBox=(selectedFlag)=>{
    if(selectedFlag.flag !==0) {
        if(selectedFlag.flag ===1 ){
            return images.checkbox
        } else {
        if (selectedFlag.checkKoate) {
            return images.checkDefult
        } else {
            return images.checkfalse
        }
        }
    } else {
            return images.checkDefult
    }
}