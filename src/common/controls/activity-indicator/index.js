import React from 'react';
import { View, Image, Text } from 'react-native';
import styles from './style';

export default AppActivityIndicator = () => {
    return (
        <View style={styles.container}>
            <View style={styles.indicator}>
                {/* <ActivityIndicator size="small" color={'gray'} /> */}
                <Image style={styles.gif} source={require('@assets/gif/loading.gif')} />
                {/* <Text style={styles.loadingText}>Loading...</Text> */}
            </View>
        </View>
    );
}
