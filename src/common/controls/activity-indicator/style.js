import { StyleSheet, Platform } from 'react-native';
import { scale, verticalScale, moderateScale } from '@src/utilities/scale';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.35)',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1000
    },
    indicator: {
        width: moderateScale(90),
        height: moderateScale(90),
        // backgroundColor: 'rgba(0, 0, 0, 0.5)',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: moderateScale(12)
    },
    loadingText: {
        paddingTop: moderateScale(5),
        color: 'white'
    },
    gif: { 
        width: moderateScale(50), 
        height: moderateScale(50) 
    }
})