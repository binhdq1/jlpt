import firebase from 'react-native-firebase';
import {Alert, AsyncStorage} from 'react-native';
const notificationService = {

    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification(async notification => {
            // Process your notification as required
        });

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const { title, body } = notificationOpen.notification;
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            // const { title, body } = notificationOpen.notification;
            // this.showAlert(title, body);
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage((message) => {
        //process data message
        });
    },

    createChannel(){
        // Build a channel
        const channel = new firebase.notifications.Android.Channel('NIHONGO-ID', 'NIHONGO Channel', firebase.notifications.Android.Importance.Max)
        .setDescription('Point channel');

        // Create the channel
        firebase.notifications().android.createChannel(channel);
    },

    showAlert(title, body) {
        Alert.alert(
        title, body,
        [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        );
    },

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    },

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        console.log('FCM TOKEN', fcmToken)
        if (!fcmToken) {
            try {
                fcmToken = await firebase.messaging().getToken();
                myTimeout = setTimeout(async() => {
                    await AsyncStorage.setItem('fcmToken', null);
                }, 3000)
                console.log('FCM TOKEN', fcmToken)
                if (fcmToken) {
                    // user has a device token
                    clearTimeout(myTimeout)
                    await AsyncStorage.setItem('fcmToken', fcmToken);
                }
            } catch (err) {
                console.log(err)
            }
        }
    },

        //2
    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }
}

export default notificationService;
