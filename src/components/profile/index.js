import React, { PureComponent } from "react"
import { Text, View, Platform, Dimensions, TouchableOpacity, Image } from "react-native"
import LinearGradient from "react-native-linear-gradient"
import * as Color from "@src/common/constants/colors"
import styles from "./styles"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import HomeHeader from '../../common/components/header-home';

class ProfileComponent extends PureComponent {

	render() {
		return (
			<LinearGradient style={styles.container} colors={[Color.SlateGray, Color.DarkCyan]}>
				<HomeHeader title={String.APP_NAME} isShowIconOut={true}/>
			</LinearGradient>
		)
	}
}

const mapStateToProps = state => {
	return {
	}
}

const mapDispatchToProps = dispatch => {
	let actionCreator = {  }
	let actions = bindActionCreators(actionCreator, dispatch)
	return { ...actions, dispatch }
}

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(ProfileComponent)
