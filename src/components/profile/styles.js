import { StyleSheet } from 'react-native'
import { moderateScale } from "@src/utilities/scale";
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex:1,
        backgroundColor: "white",
        justifyContent: 'flex-start',
    },
    avatar:{
        width:moderateScale(60),
        height:moderateScale(60),
        borderRadius:moderateScale(30),
        marginRight:moderateScale(16),
        marginLeft:moderateScale(16),

    },
    infoContainer:{
        width:"100%",
        paddingTop:moderateScale(30),
        paddingBottom: moderateScale(30),
        flexDirection: 'row',
    },
    nameContainer:{
        flex:1,
        justifyContent: 'space-around',
    },
    nameText:{
        fontSize: moderateScale(18),
        lineHeight: moderateScale(18),
        color:"#333333"
    },
    goInfoText:{
        fontSize:moderateScale(14),
        lineHeight:moderateScale(14),
        color:"#696969"
    },
    iconLeft:{
        width:moderateScale(16),
        height:moderateScale(16),
        resizeMode:'center' ,
        marginRight:moderateScale(40)
    },
    iconRight:{
        width:moderateScale(7),
        height:moderateScale(12),
    },
    menuContainer:{
        paddingLeft:moderateScale(16)
        ,paddingRight:moderateScale(16)
    },
    itemMenuContainer:{
        flexDirection:'row',
        paddingTop:moderateScale(13),
        paddingBottom:moderateScale(13),
        alignItems: 'center',
        borderBottomColor:"#f0f0f0",
        borderBottomWidth:moderateScale(1)
    },
    itemMenuText:{
        flex:1,
        fontSize: moderateScale(14),
        lineHeight:moderateScale(14),
        color:"#333333"
    },

    });

export default styles
