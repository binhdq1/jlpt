import React, { Component } from 'react';
import { View, Text, RefreshControl, ActivityIndicator } from 'react-native';
import { styles } from './styles';
import HomeHeader from '../../common/components/header-home';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

class MySchedule extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }


    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.container}>
                <HomeHeader title={String.APP_NAME} isShowIconOut={true} />
            </View>
        );
    }
}


const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    let actionCreators = { };
    let actions = bindActionCreators(actionCreators, dispatch);
    return { ...actions, dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(MySchedule);
