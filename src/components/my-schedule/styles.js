import { StyleSheet } from "react-native";
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        height: '90%'
    },
    scheduleContainer: {
        flexGrow: 1,
        justifyContent: 'space-between'
    }
});
