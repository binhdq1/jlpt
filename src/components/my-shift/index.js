import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { styles } from './styles';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { withNavigationFocus } from 'react-navigation';
class MyShift extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    render() {
        return (<View style={styles.container}>
        </View>)
    }
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    let actionCreators = {  };
    let actions = bindActionCreators(actionCreators, dispatch);
    return { ...actions, dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(MyShift));
