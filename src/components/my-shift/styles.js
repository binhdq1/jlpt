import { StyleSheet } from "react-native";
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
import { colors } from '@src/common/constants/colors';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.darkGrey
    },
    content: { 
        height: '100%' 
    },
    footer: {
        height: moderateScale(55),
    }
});
