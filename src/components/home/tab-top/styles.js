import { StyleSheet, Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
import * as Color from '@src/common/constants/colors';

const width =  Dimensions.get('window').width

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    tabView: {
		width,
		height: 0,
    },
    indicator: {
		backgroundColor: Color.Goldenrod,
		height: moderateScale(3),
	},
	tabbar: {
		zIndex: 1,
		backgroundColor: "transparent",
	},
	tabLabel: {
		fontSize: moderateScale(10),
	},
})
