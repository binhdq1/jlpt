import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import LinearGradient from 'react-native-linear-gradient';
import * as Color from '@src/common/constants/colors';
import * as String from '@src/common/constants/strings'

class TabGrammar extends React.Component {
    render(){
        return(
             <View style={{flex: 1}}>
                <LinearGradient style={styles.container} colors={[Color.SlateGray, Color.DarkCyan]}>
                    <View style ={{flex:1}}>
                        <View style={{flex: 2, alignContent: 'center'}}>
                            {this.renderItem({type:{"name":String.KANJI_N2, "count":String.COUNT_TYPE1},
                                title:String.TITLE_KANJI_N2})}
                            {this.renderItem({type:{"name":String.BUNPOU_BUN_N2, "count":String.COUNT_TYPE2},
                                title:String.TITLE_BUNPOU_BUN_N2})}
{/*
                            {this.renderItem({name:"Ngữ pháp đọc văn"})} //
*/}
                        </View>
                    </View>
                </LinearGradient>
            </View>
        )
    }
    renderItem = ({type,title}) => (
            <View style = {styles.itemContainer}>
                <TouchableOpacity style={styles.itemButton} onPress={() => this.onClickType(type,title)}>
                    <Text style = {{textAlign: 'center', fontSize: 16}}>{type.name}</Text>
                </TouchableOpacity>
            </View>
)

    onClickType = (type,title) =>{
        this.props.navigation.navigate('ExamScreen', {type: type,title:title})
    }

    _onClickItem = (name) => {

    }
}

export default TabGrammar
