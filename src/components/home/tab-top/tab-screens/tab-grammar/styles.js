import { StyleSheet, Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
import * as Color from '@src/common/constants/colors';

const width =  Dimensions.get('window').width

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    viewText: {
        flex: 1,
        justifyContent:
        'center',
        alignItems: 'center'
    },
    textHeader: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    itemContainer:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center'
    },
    itemButton:{
        height: 60,
        width: width * 0.75,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: Color.DarkGray
    }
})
