import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

import LinearGradient from 'react-native-linear-gradient';
import * as Color from '@src/common/constants/colors';

class TabRead extends React.Component {
        render(){
            return(
                 <View style={{flex: 1}}>
                    <LinearGradient style={styles.container} colors={[Color.SlateGray, Color.DarkCyan]}>
                        <View style ={{flex:1}}>
                            <View style={{flex: 2, alignContent: 'center'}}>
                                {this.renderItem("Đoạn văn ngắn")}
                                {this.renderItem("Đoạn văn trung bình")}
                                {this.renderItem("Đọc hiểu tổng hợp")}
                                {this.renderItem("Đọc hiểu chủ đề")}
                                {this.renderItem("Tìm thông tin")}
                            </View>
                        </View>
                    </LinearGradient>
                </View>
            )
        }
        renderItem = (name) => {
            return(
                <View style = {styles.itemContainer}>
                    <TouchableOpacity style={styles.itemButton} onPress={() => this._onClickItem(name)}>
                        <Text style = {{textAlign: 'center', fontSize: 16}}>{name}</Text>
                    </TouchableOpacity>
                </View>
            )
        }

        _onClickItem = (name) => {
        }
}

export default TabRead
