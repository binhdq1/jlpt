import React, { PureComponent } from 'react';
import * as Color from '@src/common/constants/colors';
import * as Strings from '@src/common/constants/strings';

import TabGrammar from './tab-screens/tab-grammar'
import TabRead from './tab-screens/tab-read'
import TabVocabulary from './tab-screens/tab-vocabulary'
import TabHomeComponent from '@src/common/components/tab-home'

import { createMaterialTopTabNavigator } from 'react-navigation';

export default TabTopHome = createMaterialTopTabNavigator({
    TabGrammar: TabGrammar,
    TabRead: TabRead,
    TabVocabulary: TabVocabulary
},{
    tabBarPosition: 'top',
    tabBarComponent: props => <TabHomeComponent {...props}/>,
    lazy: true
});
