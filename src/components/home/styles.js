import { StyleSheet } from "react-native";
import { scale, verticalScale, moderateScale } from "@src/utilities/scale";
import * as Color from '@src/common/constants/colors'
export const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        height: '85%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    scrollContainer: {
        flexGrow: 1
    },
    scrollContentContainer: {
        flex: 1,
        backgroundColor: 'transparent',
        paddingTop: moderateScale(5),
        paddingBottom: moderateScale(5),
    },
    notificationIcon: {
        marginRight: moderateScale(15)
    },
    button: {
        height: moderateScale(50),
        width: '50%',
        backgroundColor: Color.SlateGray,
        justifyContent: 'center',
        alignItems: 'center',
        margin: moderateScale(10),
    }
});
