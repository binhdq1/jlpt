import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity, Linking} from 'react-native';
import { styles } from './styles';
import HomeHeader from '../../common/components/header-home';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import LinearGradient from 'react-native-linear-gradient';
import * as Color from '@src/common/constants/colors';
import { withNavigationFocus } from 'react-navigation';
import * as String from '@src/common/constants/strings'
import NavigatorServices from '../../navigation/NavigatorServices'

class HomeScreen extends PureComponent {


    constructor(props){
        super(props);
    }
    componentDidMount() {
        Linking.addEventListener('url', this.handleOpenURL);
    }
    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleOpenURL);
    }

    handleOpenURL(event) {
        debugger
        console.log(event.url);
        // do something with the url, in our case navigate(route)
    }

    render() {

        return (
            <LinearGradient style={styles.container} colors={[Color.SlateGray, Color.DarkCyan]}>
                <HomeHeader title={String.APP_NAME} isShowIconOut={true} />
                <View style ={styles.content}>
                    <TouchableOpacity style={styles.button} onPress={() => {this.onClickType({"name":String.KANJI_N2, "count":String.COUNT_TYPE1},String.TITLE_KANJI_N2)}}>
                        <Text>Type 1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => {this.onClickType({"name":String.BUNPOU_BUN_N2, "count":String.COUNT_TYPE2},String.TITLE_BUNPOU_BUN_N2)}}>
                        <Text>Type 2</Text>
                    </TouchableOpacity>
                </View>
            </LinearGradient>
        );
    }


    onClickType = (type,title) =>{
       this.props.navigation.navigate('ExamScreen', {type: type,title:title})
    }
}




const mapStateToProps = state => {
    // storeは巨大なJsonの塊なので、nameにjsonから取って来たデータを代入している。
    return {

    }
};

const mapDispatchToProps = dispatch => {
    let actionCreators = {
    };
    let actions = bindActionCreators(actionCreators, dispatch);
    return { ...actions, dispatch };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(HomeScreen));
