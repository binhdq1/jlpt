import React, {Component} from 'react';
import {ScrollView} from 'react-native';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {withNavigationFocus} from 'react-navigation';
import StackHeader from '@src/common/components/stack-header';
import LinearGradient from 'react-native-linear-gradient';
import * as Color from '@src/common/constants/colors';
import {styles} from './styles';
import * as String from '@src/common/constants/strings'
import QuestionType1 from '@src/common/controls/N2/questionType1'
import QuestionType2 from '@src/common/controls/N2/questionType2'
import QuestionType3 from '@src/common/controls/N2/questionType3'
import QuestionType4 from '@src/common/controls/N2/questionType4'
import QuestionType5 from '@src/common/controls/N2/questionType5'
import {GET_DATA_FROM} from "../../../redux/actions";
import HTML from 'react-native-render-html';

class ExamScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checkList: null,
            isLoading: true,
            dataList: null,
            flag: true,
            ten:0,
            checkKoate: []

        }
    }

    onSuccess = (dataList, error) => {

        let checkList = [];

        let type = this.props.navigation.getParam('type').name

        if(type === String.BUNPOU_BUN_N2) {
            for (let i = 0; i < dataList.data.data[0].suggestion.length; i++) {
                checkList.push([false, false, false, false])
            }

        } else {
            for (let i = 0; i < dataList.data.data.length; i++) {
                checkList.push([false, false, false, false])
            }
        }

        this.setState({
            checkList: checkList,
            dataList: dataList.data.data,
            isLoading: false
        })

    }

    updateCheckList = (listCheckList) => {
        this.setState({
            checkList: listCheckList
        })
    }


    componentDidMount() {
        let type = this.props.navigation.getParam('type')
        this.props.getDataJLPT(type, this.onSuccess, this.onError);
    }


    onError = (error) => {
        this.setState({
            isLoading: false
        })
        alert(error)
    }

    callBackExam = () => {
        if(this.state.flag) {
            let type = this.props.navigation.getParam('type').name
            let dataList;
            if (type === String.BUNPOU_BUN_N2) {
                dataList = this.state.dataList[0]["suggestion"]
            } else {
                dataList = this.state.dataList
            }


            for (let i = 0; i < dataList.length; i++) {
                let dataListChlid = []
                let flagCheck = 0;
                let answer = parseInt(dataList[i]['answer'].charAt(dataList[i]['answer'].length - 1)) - 1
                for (let t = 0; t < this.state.checkList[i].length; t++) {
                    if (this.state.checkList[i][t] === true) {
                        if (t === answer) {
                            this.state.ten++;
                            flagCheck = 1
                            dataListChlid.push(true)

                        } else {
                            flagCheck = 2
                            dataListChlid.push(false)
                        }
                    } else {
                        if (t === answer) {
                            dataListChlid.push(true)

                        } else {
                            dataListChlid.push(null)
                        }
                    }
                }
                this.state.checkKoate.push({checkKoate: dataListChlid, flag: flagCheck})
            }

            this.setState({
                flag: false
            })
        }
    }


    render() {

        let html = '<div style="margin-top: 10px;margin-bottom: 10px;margin-left: 5px">' + this.props.navigation.getParam('title') + '</div>'
        let Question;
        let type = this.props.navigation.getParam('type').name
        switch (type) {
            case String.KANJI_N2:
                Question = QuestionType1;
                break;

            case String.GOI_N2:
                Question = QuestionType1;
                break;

            case String.Goi_SET_N2:
                Question = QuestionType1;
                break;

            case String.BUNPOU_BUN_N2:
                Question = QuestionType5;
                break;

            case String.KUMITATE_N2:
                Question = QuestionType4;
                break;

            case String.Kanji_Revert_N2:
                Question = QuestionType1;
                break;

            case String.BUNPOU_N2:
                Question = QuestionType1;
                break;

            case String.SYNONYM_N2:
                Question = QuestionType2;
                break;

            case String.USAGE_N2:
                Question = QuestionType3;
                break;

        }
        return (
            <LinearGradient style={styles.container} colors={[Color.SlateGray, Color.DarkCyan]}>
                {this.state.isLoading && <AppActivityIndicator/>}
                <StackHeader props={this.props.navigation}
                             title={{ten:this.state.ten,type: type, titleRight: String.TITLE_RIGHT, callBack: this.callBackExam}}/>
                <ScrollView style={styles.content}>
                    <HTML html={html}></HTML>
                    {this.state.dataList !== null &&
                    <Question item={this.state.dataList} name={type} checkList={this.state.checkList}
                              updateCheckList={this.updateCheckList} flag={this.state.flag} checkKoate={this.state.checkKoate}/>}
                </ScrollView>
            </LinearGradient>
        )
    }


}


const mapStateToProps = state => {
    return {}
};

const mapDispatchToProps = dispatch => {
    let actionCreators = {
        getDataJLPT: (body, onSuccess, onError) => dispatch(GET_DATA_FROM(body, onSuccess, onError)),
    };
    let actions = bindActionCreators(actionCreators, dispatch);
    return {...actions, dispatch};
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(ExamScreen));
