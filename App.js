/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */


import { Provider } from 'react-redux'
import React from 'react';
import { Root } from "native-base";
import AppNavigator from '@src/navigation';
import NavigationService from '@src/navigation/NavigatorServices';
import ModalLogout from '@src/common/components/modal-logout';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from './src/redux/config';

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Root>
                    <PersistGate loading={null} persistor={persistor}>
                        <AppNavigator
                            ref={
                                navigatorRef => {
                                    NavigationService.setTopLevelNavigator(navigatorRef);
                                }
                            }
                        />
                        <ModalLogout/>
                    </PersistGate>
                </Root>
            </Provider>
        )
    }
    componentDidMount() {
    }
}
